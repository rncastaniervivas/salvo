package com.codeoftheweb.salvo;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Salvo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long idSalvo;
    private int turn;

    //Relacion manytoone con gamePlayer porque un jugador TIENE muchos salvos
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gamePlayer_id")
    private GamePlayer gamePlayer;

    @ElementCollection
    @Column(name = "salvoLocationList")
    private List<String> salvoLocationList = new ArrayList<>();

    //constructor vacio por default y constructor con parametros
    public Salvo (){

    }

    public Salvo (int turn, GamePlayer gamePlayer, List<String> salvoLocationList){
        this.setTurn(turn);
        this.setGamePlayer(gamePlayer);
        this.setSalvoLocationList(salvoLocationList);
    }

    //getters y setters de Salvo

    public Long getIdSalvo() {
        return idSalvo;
    }

    public void setIdSalvo(Long idSalvo) {
        this.idSalvo = idSalvo;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    public List<String> getSalvoLocationList() {
        return salvoLocationList;
    }

    public void setSalvoLocationList(List<String> salvoLocationList) {
        this.salvoLocationList = salvoLocationList;
    }
}

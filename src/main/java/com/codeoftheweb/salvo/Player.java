package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;



@Entity
public class Player{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String userName ;

    private String password;

    //la diferencia entre set (mas nuevo y con mas funciones para no admitir repetidos) y list (menos funciones y admite y puede mostrar datos repetidos)
    //a nivel de tiempo de ejecucion tal vez el Set es mas eficiente
    @OneToMany(mappedBy="player", fetch=FetchType.EAGER)
    private Set<GamePlayer> gamePlayerSet;

    //desde donde voy, hacia donde voy
    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<Score> scoreSet;

    //genero un constructor con el tipo de dato que le voy a pasar
    public Player() { }

    public Player(String userName, String password) {

        this.setUserName(userName);
        this.setPassword(password);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //soportar la relacion de ambas tablas. Voy a tener los id de los objetos que tengo, en este caso PLayer y Game
    @JsonIgnore
    public void addGamePlayer(GamePlayer gameplayer){
        gameplayer.setPlayer(this);
        getGamePlayerSet().add(gameplayer);
    }

    @JsonIgnore
    public Set<GamePlayer> getGamePlayerSet() {
        return gamePlayerSet;
    }

    public void setGamePlayerSet(Set<GamePlayer> gamePlayerSet) {
        this.gamePlayerSet = gamePlayerSet;
    }

    public Set<Score> getScoreSet() {
        return scoreSet;
    }

    public void setScoreSet(Set<Score> scoreSet) {
        this.scoreSet = scoreSet;
    }

    public long getWins(){
        return this.scoreSet.stream().filter(score -> score.getScore() == 1.00) .count();

    }

    public long getTies(){
        return this.scoreSet.stream().filter(score -> score.getScore() == 0.50) .count();

    }

    public long getLoses(){
        // this.scoreSet (llamo al set que cree de tipo SCORE .stream (coleccion de datos
        // .filter() filtra los resultados del tipo que necesitamos y lo comparamos con el valor necesario
        // con el .count() contamos cuantas veces sucede esto
        return this.scoreSet.stream().filter(score -> score.getScore() == 0.00) .count();

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

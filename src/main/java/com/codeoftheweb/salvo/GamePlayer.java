package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class GamePlayer {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private Date joinDate;

    //relacion manytoone porque en Player hay una relacion onetomany
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name= "player_id")
    private Player player;

    //relacion manytoone porque en Game hay una relacion onetomany
    @ManyToOne (fetch=FetchType.EAGER)
    @JoinColumn(name ="game_id")
    private Game game;

    //relacion onetomany de gamePlayer con ship
    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private Set<Ship> ships;

    //relacion onetomany de gamePlayer con los salvoes (genero sus getters y setters)
    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private Set<Salvo> salvoes;



    public GamePlayer(){

    }

    public GamePlayer (Date creationDate, Game game, Player player){
        this.setJoinDate(creationDate);
        this.game = game;
        this.player = player;
    }


    @JsonIgnore
    public Player getPlayer() {
        return player;
    }

    //agrego dos etiquetas por bucle
    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public long getId() {
        return id;
    }


    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }


    @JsonIgnore
    public void addShip( Ship ship){
        ship.setGamePlayer(this);
        getShips().add(ship);
    } //descomente el json ignore de este metodo

    public Set<Ship> getShips() {
        return ships;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }


    public Set<Salvo> getSalvoes() {
        return salvoes;
    }

    public void setSalvoes(Set<Salvo> salvoes) {
        this.salvoes = salvoes;
    }

    public Score getScore(){
        return this.player.getScoreSet().stream().findFirst().orElse(null);
    }

    //genero un opponent de tipo GamePlayer
    public GamePlayer getOppPlayer (GamePlayer gamePlayer){
        GamePlayer oppGamePlayer = gamePlayer.getGame().getGamePlayerSet()
                //getId() estaba getGamePlayerId()
                .stream().filter(gp->gp.getId() != gamePlayer.getId())
                .findFirst()
                .get();
        return oppGamePlayer;
    }
}

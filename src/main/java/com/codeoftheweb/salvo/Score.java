package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator ="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private double score;
    private Date finishDate;

    //siempre del lado many tengo que tener el joincolumn
    @ManyToOne (fetch=FetchType.EAGER)
    @JoinColumn(name ="game_id")
    private Game game;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name= "player_id")
    private Player player;

    public Score(){

    }

    public Score(Game game1, Player player1, double score, Date finishDate){
        this.setGame(game1);
        this.setPlayer(player1);
        this.setScore(score);
        this.setFinishDate(finishDate);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    //comento con jsonignore
    @JsonIgnore
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @JsonIgnore
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }


    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}

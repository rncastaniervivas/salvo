package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Ship {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String shipType;

    //relacion ManyToOne con el gamePlayerId
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "gamePlayer_id")
    private GamePlayer gamePlayer;


    /*la anotacion nos evita crear una clase innecesaria.
    No sirve la clase que tenga al menos un elemento dintintivo y al menos un atributo
    Lo mismo habra que hacer en Salvo */
    @ElementCollection
    @Column(name = "locationList")
    private List<String> locationList = new ArrayList<>();

    public Ship(){

    }

    public Ship(String shipType, GamePlayer gamePlayer, List<String> locationList){

        this.setShipType(shipType);
        this.setGamePlayer(gamePlayer);
        this.setLocationList(locationList);

    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShipType() {
        return shipType;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    @JsonIgnore
    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    public List<String> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<String> locationList) {
        this.locationList = locationList;
    }
}

package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//Punto de partida del programa, aca es donde se guarda la persistencia y el hardcodeo

@SpringBootApplication
public class SalvoApplication {

	//punto de partida de la clase estatica
	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	//con la anotacion Bean spring se encarga de configurar los recursos de cuando se instancia los objetos y como interactuan.
	@Bean
	public CommandLineRunner initData(PlayerRepository playerrepository, GameRepository gamerepository, GamePlayerRepository gameplayerrepository, ShipRepository shiprepository, SalvoRepository salvorepository, ScoreRepository scorerepository) {
		return (args) -> {



			//creo todos los jugadores
			Player p1 = new Player("Rocio@gmail.com", passwordEncoder().encode("1"));
			Player p2 = new Player("Morita@gmail.com", passwordEncoder().encode("2"));
			Player p3 = new Player("Julian@gmail.com", passwordEncoder().encode("3"));
			Player p4 = new Player("Agustin@gmail.com", passwordEncoder().encode("4"));

			playerrepository.save(p1);
			playerrepository.save(p2);
			playerrepository.save(p3);
			playerrepository.save(p4);

			//creo todas las nuevas fechas y los juegos con esas fechas creadas
			Date f1 = new Date();
			Game g1 = new Game(f1);
			Date f2 = new Date();
			Game g2 = new Game(f2);
			Date f3 = new Date();
			Game g3 = new Game(f3);
			Date f4 = new Date();
			Game g4 = new Game(f4);

			gamerepository.save(g1);
			gamerepository.save(g2);
			gamerepository.save(g3);
			gamerepository.save(g4);

			//creo todos los gamePlayer con los players, la fecha y el juego
			GamePlayer gp1 = new GamePlayer(f1, g1, p1);
			GamePlayer gp2 = new GamePlayer(f2, g2, p2);
			GamePlayer gp3 = new GamePlayer(f3, g3, p3);
			GamePlayer gp4 = new GamePlayer(f4, g4, p4);
			GamePlayer gp5 = new GamePlayer(f1, g1, p2);
			GamePlayer gp6 = new GamePlayer(f2, g2, p3);

			gameplayerrepository.save(gp1);
			gameplayerrepository.save(gp2);
			gameplayerrepository.save(gp3);
			gameplayerrepository.save(gp4);
			gameplayerrepository.save(gp5);
			gameplayerrepository.save(gp6);

			//genero ubicaciones antes que los barcos
			List<String> locationList = new ArrayList<>();
			locationList.add("H1");
			locationList.add("H3");
			locationList.add("H2");
			locationList.add("H4");
			locationList.add("H5");

			//genero lista del turno con los salvos del primer jugador
			List<String> locationSalvoList = new ArrayList<>();
			locationSalvoList.add("A1");
			locationSalvoList.add("A3");
			locationSalvoList.add("A2");
			locationSalvoList.add("A4");
			locationSalvoList.add("A5");

			//genero lista del turno con los salvos del segundo jugador
			List<String> locationSalvoList2 = new ArrayList<>();
			locationSalvoList2.add("B1");
			locationSalvoList2.add("B3");
			locationSalvoList2.add("B2");
			locationSalvoList2.add("B4");
			locationSalvoList2.add("B5");


			//creo y guardo nuevos barcos
			//ubicacion H1 H2 H3 H4 H5
			Ship sh1= new Ship("army", gp1, locationList);

			//Ubicacion A1 A2 A3 A4 A5
			Ship sh2= new Ship("submarino", gp5, locationSalvoList);

			shiprepository.save(sh1);
			shiprepository.save(sh2);



			//creo y guardo los nuevos salvos
			Salvo salvo = new Salvo(10, gp1, locationSalvoList2);
			Salvo salvo2 = new Salvo(11, gp5, locationList);

			salvorepository.save(salvo);
			salvorepository.save(salvo2);

			//creo los nuevos score

			Date finishDate = new Date();
			Score score1 = new Score(g1, p1,1,finishDate);
			Score score2 = new Score(g1, p2, 0.5, finishDate);
			Score score3 = new Score(g2, p1,1,finishDate);
			Score score4 = new Score(g3, p3, 0, finishDate);
			Score score5 = new Score(g4, p4, 1, finishDate);

			scorerepository.save(score1);
			scorerepository.save(score2);
			scorerepository.save(score3);
			scorerepository.save(score4);
			scorerepository.save(score5);

			//guardo un puntaje para cada jugado para poder solucionar el problema de la tabla que no se muestra

			};
	}
	@Bean
	public PasswordEncoder passwordEncoder(){
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}
}


@EnableWebSecurity
@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter{

	@Autowired
	PlayerRepository playerRepository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(inputName -> {
			Player player = playerRepository.findByUserName(inputName);
			if(player != null){
				return new User(player.getUserName(), player.getPassword(),
						AuthorityUtils.createAuthorityList("USER"));
				} else{
				throw new UsernameNotFoundException("El usuario no existe" + inputName);
					}
		});
	}
}

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	@Override
	protected void configure (HttpSecurity http) throws Exception {
		http.authorizeRequests()
				//cambie games_3.html sacando el _3
				.antMatchers("/web/games.html").permitAll()
				.antMatchers("/web/**").permitAll()
				.antMatchers("api/games").permitAll()
				.antMatchers("/api/games").permitAll()
				.antMatchers("/api/players").permitAll()
				.antMatchers("/api/game_view/*").hasAuthority("USER")
				.antMatchers("/rest/*").denyAll()
				.anyRequest().permitAll();

		http.formLogin()
				.usernameParameter("name")
				.passwordParameter("pwd")
				.loginPage("/api/login");

		http.logout().logoutUrl("/api/logout");

		// turn off checking for CSRF tokens
		http.csrf().disable();

		// if user is not authenticated, just send an authentication failure response
		http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if login is successful, just clear the flags asking for authentication
		http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

		// if login fails, just send an authentication failure response
		http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if logout is successful, just send a success response
		http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
	}

	private void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		}
	}

}



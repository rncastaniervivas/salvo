package com.codeoftheweb.salvo;


import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;



//le avisamos que va a recibir un JSON
@RestController
@RequestMapping("/api") //agrega /api a todas las URLs para este controller
public class SalvoController {

    //hago un get de game repository
    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    //@RequestMapping("/games")
    public List<Object> getAll(){
        return gameRepository
                .findAll()
                .stream()
                .map(game -> GameDTO(game))
                .collect(Collectors.toList());
    }

    @RequestMapping("/games")
    public Map<String, Object> makeLoggedDTO (Authentication authentication){
        Map<String, Object> dto = new LinkedHashMap<>();
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
            dto.put("player", "Guest");
        else
            dto.put("player", loggedPlayerDTO(playerRepository.findByUserName(authentication.getName())));
            dto.put("games", getAll());
            return dto;
    }

    //luego de generar el nuevo requestMapping de /games genero el Map de logged
    public Map<String, Object> loggedPlayerDTO(Player player){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("name", player.getUserName());
        return dto;
    }

    //aca es donde tengo que cambiar esta accion por la que esta en slack
    @RequestMapping("/game_view/{id}")
    public  Map<String,Object> getGameView(@PathVariable long id){
        return gameViewDTO(gamePlayerRepository.findById(id).get());
    }



    //nuevo mapping con POST
    @RequestMapping(path = "/game_view/{id}", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getGameView(Authentication auth, @PathVariable long id){
        if(gamePlayerRepository.findById(id) == null){
            return new ResponseEntity<>(makeMap("error", "Game not found"), HttpStatus.FORBIDDEN);
        }
        if(gamePlayerRepository.findById(id).get().getPlayer().getUserName() == auth.getName()){
            return new ResponseEntity<>(gameViewDTO(gamePlayerRepository.findById(id).get()), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(makeMap("error", "Unathorized"), HttpStatus.UNAUTHORIZED);
    }


    //el mapping que con el id del game te muestra los jugadores y pongo restricciones para q accedan a sus propios juegos
    @RequestMapping(path = "/game/{id}/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>>joinGame (@PathVariable long id, Authentication authentication){
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
            return new ResponseEntity<>(makeMap("error", "Unathorized"), HttpStatus.UNAUTHORIZED);
        if(gameRepository.findById(id) == null)
            return new ResponseEntity<>(makeMap("error", "Forbidden"), HttpStatus.FORBIDDEN);

        if (gameRepository.findById(id).get().getGamePlayerSet().size() > 2 )
            return new ResponseEntity<>(makeMap("error", "Unathorized"), HttpStatus.UNAUTHORIZED);

        GamePlayer gamePlayer= new GamePlayer();
        gamePlayer.setPlayer(playerRepository.findByUserName(authentication.getName()));
        gamePlayer.setGame(gameRepository.findById(id).get());
        gamePlayerRepository.save(gamePlayer);

        return new ResponseEntity<>(makeMap("gpid", gamePlayer.getId()), HttpStatus.CREATED);
    }

    //metodo makeMap
    private Map<String, Object> makeMap(String key, Object value){
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(key, value);
        return map;
    }


    //url /players
    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestParam String username,
                                           @RequestParam String password){
        if(username.isEmpty() || password.isEmpty()){
            return new ResponseEntity<>("Missing data", HttpStatus.FORBIDDEN);
        }
        if (playerRepository.findByUserName(username) != null){
            return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
        }
        playerRepository.save(new Player(username, passwordEncoder.encode(password)));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    //request mapping task 3.1
    @RequestMapping(path = "games/players/{gamePlayerId}/ships", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> addShips (@PathVariable Long gamePlayerId, Authentication authentication, @RequestBody Set <Ship> ships){
        GamePlayer gamePlayer = gamePlayerRepository.getOne(gamePlayerId);
        Player player = playerRepository.findByUserName(authentication.getName());


        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
            return  new ResponseEntity<>(makeMap ("Unathorized", ""), HttpStatus.UNAUTHORIZED);
        if(gamePlayer == null)
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if(gamePlayer.getPlayer().getUserName() == authentication.getName())
            return new ResponseEntity<>(makeMap ("Not the same UserName",""), HttpStatus.FORBIDDEN);
        //logica a comprobar si TODOS los barcos fueron colocados en el mismo turno
        if(gamePlayer.getShips().size() >= 5)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        for ( Ship ship : ships){
            ship.setGamePlayer(gamePlayer);
            shipRepository.save(ship);
        }
        return new ResponseEntity<>(makeMap("OK", "Ships placed"), HttpStatus.OK);
    }


    //request mapping task 4 similar al 3 pero con salvoes
    @RequestMapping(path = "games/players/{gamePlayerId}/salvoes", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> addSalvoes (@PathVariable Long gamePlayerId, Authentication authentication, @RequestBody Set <Salvo> salvoes){
        GamePlayer gamePlayer = gamePlayerRepository.getOne(gamePlayerId);
        Player player = playerRepository.findByUserName(authentication.getName());


        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
            return  new ResponseEntity<>(makeMap ("Unathorized", ""), HttpStatus.UNAUTHORIZED);
        if(gamePlayer == null)
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        if(gamePlayer.getPlayer().getUserName() == authentication.getName())
            return new ResponseEntity<>(makeMap ("Not the same UserName",""), HttpStatus.FORBIDDEN);
        //logica a comprobar si TODOS los salvos fueron colocados en el mismo turno
        if(gamePlayer.getShips().size() >= 5)
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        for ( Salvo salvo : salvoes){
            salvo.setGamePlayer(gamePlayer);
            salvoRepository.save(salvo);
        }
        return new ResponseEntity<>(makeMap("OK", "Salvoes placed"), HttpStatus.OK);
    }

    /*@RequestMapping(path = "/game_view/{gamePlayerId}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getGamePlayerData (@PathVariable Long gamePlayerId, Authorization authorization) {
        GamePlayer user = gamePlayerRepository.findOne(gamePlayerId);
        GamePlayer opponent =

    }*/

    //url que muestra los leaderboards
    @RequestMapping("/leaderBoard")
    public List<Object> getLeaderboard(){
        return playerRepository
                .findAll()
                .stream()
                .map(player-> leaderboardDTO(player))
                .collect(Collectors.toList());
    }


    //Request mapping para

    //dto del leaderboard
    private Map<String, Object> leaderboardDTO (Player player){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("name", player.getUserName());
        dto.put("score", scoreDTO(player));
        return dto;
    }
    //
    private Map<String,Object> gameViewDTO(GamePlayer gamePlayer) {
        Map<String,Object> dto = new LinkedHashMap<>();
        dto.put("id",gamePlayer.getGame().getId());
        dto.put("creationDate",gamePlayer.getGame().getCreationDate());
        dto.put("gamePlayers", getGamePlayerList(gamePlayer.getGame().getGamePlayerSet()));
        dto.put ("ships", getShipList(gamePlayer.getShips()));
        dto.put("salvoes", obtainAllSalvoes(gamePlayer));
        dto.put("hits", makeHitsDTO(gamePlayer, gamePlayer.getOppPlayer(gamePlayer)));

        return dto;
    }

    //funcion de mapeo del jugador
    private Map<String, Object> playerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("userName", player.getUserName());
        dto.put("score", scoreDTO(player));
        return dto;
    }

    //funcion de mapero de gamePlayer
    private Map<String, Object> gamePlayerDTO(GamePlayer gamePlayer){
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", gamePlayer.getId());
        dto.put("player", playerDTO(gamePlayer.getPlayer()));
        return dto;

    }

    //funcion de mapeo de la partida
    private Map<String, Object> GameDTO(Game game){
        Map <String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", game.getId());
        dto.put("creationDate", game.getCreationDate());
        dto.put("gamePlayers", getGamePlayerList(game.getGamePlayerSet()));
        return dto;
    }

    //funcion de mapeo de los barcos
    private Map<String, Object> shipDTO(Ship ship){
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("type", ship.getShipType());
        dto.put("locations", ship.getLocationList());
        return dto;
    }

    /*genero el dto del salvo donde se le guarda el turno y la ubicacion a donde disparo
     * Task 4 punto 1*/
    private Map<String, Object> salvoDTO(Salvo salvo){
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        dto.put("locations", salvo.getSalvoLocationList());
        return dto;
    }

    //preguntar si es necesario hacer un dto del conjunto de salvos
    private Map<String, Object> salvoesDTO (Set<Salvo> salvoes){
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("salvoes", salvoes.stream().map(sv-> salvoDTO(sv)).collect(Collectors.toList()));
        return dto;
    }

    /*genero el dto de score donde se guarda el puntaje de la partida1*/
    private Map<String, Object> scoreDTO (Player player){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("total", player.getWins() + player.getTies()* 0.5);
        dto.put("won", player.getWins());
        dto.put("tied", player.getTies());
        dto.put("lost", player.getLoses());
        return dto;

    }


    //dto de hits
    private Map<String, Object> makeHitsDTO(GamePlayer selfGP, GamePlayer opponentGP){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(selfGP, opponentGP));
        dto.put("opponent", getHits(opponentGP, selfGP));
        return dto;
    }

    //metodo getHits
    private List<Map> getHits(GamePlayer gamePlayer, GamePlayer opponentGameplayer) {

        List<Map> hits = new ArrayList<>();
        Integer carrierDamage = 0;
        Integer battleshipDamage = 0;
        Integer submarineDamage = 0;
        Integer destroyerDamage = 0;
        Integer patrolboatDamage = 0;

        List <String> carrierLocation = new ArrayList<>();
        List <String> battleshipLocation = new ArrayList<>();
        List <String> submarineLocation = new ArrayList<>();
        List <String> destroyerLocation = new ArrayList<>();
        List <String> patrolboatLocation = new ArrayList<>();

        gamePlayer.getShips().forEach(ship -> {
            switch (ship.getShipType()) {
                case "carrier":
                    carrierLocation.addAll(ship.getLocationList());
                    break;

                case "battleship":
                    battleshipLocation.addAll(ship.getLocationList());
                    break;

                case "submarine":
                    submarineLocation.addAll(ship.getLocationList());
                    break;

                case "destroyer":
                    destroyerLocation.addAll(ship.getLocationList());
                    break;

                case "patrolboat":
                    patrolboatLocation.addAll(ship.getLocationList());
                    break;

            }

        });

        for (Salvo salvo : opponentGameplayer.getSalvoes()) {
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0;
            Integer missedShots = salvo.getSalvoLocationList().size();

            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();

            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();
            salvoLocationsList.addAll(salvo.getSalvoLocationList());

            for (String salvoShot : salvoLocationsList) {
                if (carrierLocation.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }

                if (battleshipLocation.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }

                if (submarineLocation.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }

                if (destroyerLocation.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }

                if (patrolboatLocation.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }

            }

            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("carrier", carrierDamage);
            damagesPerTurn.put("battleship", battleshipDamage);
            damagesPerTurn.put("submarine", submarineDamage);
            damagesPerTurn.put("destroyer", destroyerDamage);
            damagesPerTurn.put("patrolboat", patrolboatDamage);
            hitsMapPerTurn.put("turn", salvo.getTurn());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);

            hits.add(hitsMapPerTurn);

        }

        return hits;

    }

    //obtengo una lista a partir de un set
    private List<Object> getGamePlayerList(Set<GamePlayer> gamePlayers){
        return gamePlayers.stream().map(gamePlayer->gamePlayerDTO(gamePlayer))
                .collect(Collectors.toList());
    }

    private List<Map<String, Object>> getShipList(Set<Ship> ships){
        return ships
                .stream()
                .map(ship -> shipDTO(ship))
                .collect(Collectors.toList());
    }

    //

    //lista de todos los barcos de tipo set
   /* private List<Map<String, Object>> obtainAllShips(GamePlayer gamePlayer){
        List<Map<String,Object>> shipList = new ArrayList<>();
        gamePlayer.getGame().getGamePlayerSet().forEach(gpl -> gpl.getShips().forEach(ship -> shipList.add(shipDTO(ship))));
        return shipList;
    }*/


    //Obtengo una lista de salvos a partir de un set
    private List<Map<String, Object>> obtainAllSalvoes(GamePlayer gamePlayer){
        List<Map<String,Object>> salvoList = new ArrayList<>();
        gamePlayer.getGame().getGamePlayerSet().forEach(gpl -> gpl.getSalvoes().forEach(salvo -> salvoList.add(salvoDTO(salvo))));
        return salvoList;
    }
}
